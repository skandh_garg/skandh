#!/usr/bin/env bash

echo -e "In the bootstrap file\n\n\n\n\n\n"

sudo apt-get -y update

echo -e "RUNNING APT-GET UPGRADE NOW"
#apt-get upgrade -y
#sudo dpkg --configure -a


######################################################
echo -e "Installing Oracle Java 8\n\n\n\n\n\n"
#Installing Oracle 8
sudo apt-get install -y software-properties-common python-software-properties
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo add-apt-repository ppa:webupd8team/java -y
sudo apt-get update
sudo apt-get install oracle-java8-installer
echo "Setting environment variables for Java 8.."
sudo apt-get install -y oracle-java8-set-default



#######################################################
# Installing Apache

sudo apt-get -y install apache2

# Installing MySQL and it's dependencies, Also, setting up root password for MySQL as it will prompt to enter the password during installation

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password '
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password '
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql
########################################################




echo -e "Installing Tomcat version 9\n\n\n\n\n\n"
cd /opt
sudo wget http://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.0.M22/bin/apache-tomcat-9.0.0.M22.tar.gz
sudo tar xzf apache-tomcat-9.0.0.M22.tar.gz
sudo mv apache-tomcat-9.0.0.M22 tomcat9
sudo "export CATALINA_HOME="/opt/tomcat9"" >> /etc/environment
sudo echo "export JAVA_HOME="/usr/lib/jvm/java-8-oracle"" >> /etc/environment
sudo echo "export JRE_HOME="/usr/lib/jvm/java-8-oracle/jre"" >> /etc/environment
echo "DONE WITH VARIABLES\n\n\n"
source ~/.bashrc
cd /opt/tomcat9/conf
sed -i 's|</tomcat-users>|<!-- user manager can access only manager section -->\\\n<role rolename="manager-gui" />\\\n<user username="manager" password="_SECRET_PASSWORD_" roles="manager-gui" />\\\n<!-- user admin can access manager and admin section both -->\\\n<role rolename="admin-gui" />\\\n<user username="admin" password="_SECRET_PASSWORD_" roles="manager-gui,admin-gui" />&|' tomcat-users.xml
cd /opt/tomcat9
./bin/startup.sh