package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/testing")
public class UserController {

	private UserRepository userRepository;
	private final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	public UserController(UserRepository userRepository){
		this.userRepository=userRepository;
	}
	
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public @ResponseBody Iterable<User>  getAll(HttpServletRequest request) {

		logger.error(" "+request.getRequestURI()+"?"+request.getQueryString()+" "+request.getRemoteAddr()+" ");
		
		User user = new User();
		Name name= Name.Abhay;
		user.setName(name);
		userRepository.save(user);
		
		
		User user2= new User();
		Name name2= Name.Skandh;
		user2.setName(name2);
		userRepository.save(user2);
		
		return userRepository.findAll();
	}
	
}
